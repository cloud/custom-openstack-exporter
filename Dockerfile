FROM registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/docker.io__almalinux:9

ARG VERSION=unknown-version
ARG BUILD_DATE=unknown-date
ARG CI_COMMIT_SHA=unknown
ARG CI_BUILD_HOSTNAME
ARG CI_BUILD_JOB_NAME
ARG CI_BUILD_ID

COPY requirements.pip dependencies.yum.txt /tmp/
COPY custom-openstack-exporter.py timeout.py /usr/local/bin/

RUN yum -y install $(cat /tmp/dependencies.yum.txt) && \
    yum -y update

RUN pip3 install --upgrade pip && \
    pip3 --no-cache-dir install --upgrade $(cat /tmp/requirements.pip)

ENTRYPOINT ["/usr/local/bin/custom-openstack-exporter.py"]

LABEL maintainer="MetaCentrum Cloud Team <cloud[at]ics.muni.cz>" \
    org.label-schema.schema-version="1.0.0-rc.1" \
    org.label-schema.vendor="Masaryk University, ICS" \
    org.label-schema.name="custom-openstack-exporter" \
    org.label-schema.version="$VERSION" \
    org.label-schema.build-date="$BUILD_DATE" \
    org.label-schema.build-ci-job-name="$CI_BUILD_JOB_NAME" \
    org.label-schema.build-ci-build-id="$CI_BUILD_ID" \
    org.label-schema.build-ci-host-name="$CI_BUILD_HOSTNAME" \
    org.label-schema.url="https://gitlab.ics.muni.cz/cloud/custom-openstack-exporter" \
    org.label-schema.vcs-url="https://gitlab.ics.muni.cz/cloud/custom-openstack-exporter" \
    org.label-schema.vcs-ref="$CI_COMMIT_SHA"
