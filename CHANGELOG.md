# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.5.3] - 2023-11-27
### Fix
- More precise visibility rules

## [1.5.2] - 2023-11-23
### Fix
- Catch empty SG config value

## [1.5.1] - 2023-11-23
### Fix
- Remove too strict condition

## [1.5.0] - 2023-11-22
### Added
- Heuristics (remember past successful connection method)
- Exclude security group rules, if the exporter can't reach the VM

## [1.4.2] - 2023-10-18
### Fix
- Use almalinux 9 (instead of 8) to fix python dependencies

## [1.4.1] - 2023-10-18
### Fix
- Skip loadbalancer checks if loadbalancer endpoint is missing

## [1.4.0] - 2023-08-28
### Added
- New metrics custom_openstack_server_info and custom_openstack_flavor_info

## [1.3.0] - 2023-04-11
### Fix
- Adding HTTP + HTTPS port checking

## [1.2.2] - 2023-03-06
### Fix
- Attempt to open VM connection, only if tcp is allowed in security rule
- Make exceptions version independent

## [1.2.1] - 2023-03-01
### Fix
- Changed socket exception name for python 3.6 (almalinux uses older version of python)

## [1.2.0] - 2023-02-28
### Added
- Logging
- Connection to target ports tested with socket (HTTP GET before)

## [1.1.5] - 2023-02-15
### Update
- Broken into more smaller functions, changed flow
- Ping only if icmp is allowed
- Pinging in separate func and tried before ports
- Deal with cases when security rule ranges are None

## [1.1.4] - 2023-02-10
### Added
- New var PORT_DEFAULT_RDP added to Constants
- Changed get_port() to support Windows machines (RDP port check as well as SSH, only SSH before)
- get_port() returns list (integer before)

## [1.1.3] - 2022-05-10
### Update
- Refactor ping usage
- Allow user to specify timeout for api calls
- Refactor ip_status metrics holding
- Make loadbalancer default port dependent on port range

## [1.1.2] - 2022-05-10
### Update
- Hold ip status metrics during evaluation
- Add 1025 port for lb testing by default

## [1.1.1] - 2022-05-04
### Fixed
- Catching missing security groups for vms
- Increased timeout for api reply to 2mins
- Reverted hidden lb status - not necessary

## [1.1.0] - 2022-04-22
### Added
- Added tenant public ip monitoring

## [1.0.3] - 2022-04-21
### Added
- Added custom project info

## [1.0.2] - 2022-03-28
### Update
- Updated loadbalancer monitoring

## [1.0.1] - 2022-03-14
### Fixed
- Fixed copying script

## [1.0.0] - 2022-02-18
### Added
- Initial release
