# custom-openstack-exporter

Script custom-openstack-exporter.py needs application credential in cloud.yaml file. 
If option --os-cloud is not used then the variable OS_CLOUD takes place. If none is set
the default is 'openstack' for cloud.

## Example use

```
./custom-openstack-exporter.py --os-cloud stage --address localhost --port 8080 --interval 120
```

## What makes a VM `HIDDEN`

VM has no valid security group **OR** rules inside a valid security group/s were excluded

Reasons for exclusion:
1. Direction isn't `ingress`.
1. VM access is limited to a subnet, which the exporter isn't part of.
1. Rule's `remote_ip_prefix` isn't a subnet of specified visible networks.
1. Rule's `remote_ip_prefix` is a subnet of specified excluded networks (subnets of visible networks).
1. Access is limited to a VM with a specific security group.
1. Rule's `protocol` doesn't make sense in the context of the connection method.
