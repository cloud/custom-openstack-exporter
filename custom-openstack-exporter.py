#!/usr/bin/env python3

"""
Custom openstack prometheus exporter

Example usage:
./custom-openstack-exporter.py --os-cloud stage --address localhost --port 8080 --interval 120

To start in debug mode:
./custom-openstack-exporter.py --address localhost --port 8080 --interval 120 --log-level "debug"
"""

import os
import time
import argparse
import ipaddress
import enum
import socket
import logging

from prometheus_client import start_http_server, Gauge, REGISTRY, PROCESS_COLLECTOR, PLATFORM_COLLECTOR
from ping3 import ping
import openstack
import keystoneauth1.exceptions.catalog as ks_exc

import timeout

LOG_LEVELS = {
    'debug':logging.DEBUG,
    'info':logging.INFO,
    'warning':logging.WARNING,
    'error':logging.ERROR,
    'critical':logging.CRITICAL}

class IPAddressStatus(enum.Enum):
    """
    Representation of IP address status
    """

    REACHABLE = 'REACHABLE'
    HIDDEN = 'HIDDEN'
    UNREACHABLE = 'UNREACHABLE'
    UNKNOWN = 'UNKNOWN'

class Constants:
    ''' Miscellaneous constant variables '''
    PING_TIMEOUT = 0.5
    CONNECTION_TIMEOUT = 1
    PORTS_COMMON = [22, 3389, 80, 443]
    PORTS_ADDITIONAL_NUM = 2
    ICMP_PROTOCOLS = ['any', 'icmp', 'ipv6-icmp']
    TCP_PROTOCOLS = ['any', 'tcp']

class AppMetrics:
    """
    Representation of Prometheus metrics and loop to fetch and transform
    application metrics into Prometheus metrics.
    """

    loadbalancer_accessibility_ports = range(1025, 1050)
    virtual_machines = []
    security_groups = []
    fetch_ip_clear_cache = []
    # REACHABLE port or ping from previous run saved as str, 'IP':'PORT' or 'IP':'ping'
    reachable_past_run = {}

    def __init__(self, args):
        self.polling_interval_seconds = args.interval
        self.api_timeout = args.ip_status_api_timeout
        self.os_connection = openstack.connect(cloud=args.os_cloud)
        self.ipv4_monitor_visibility = [ipaddress.ip_network(prefix.strip()) for prefix in args.ip_status_ipv4_monitor_visibility.split(",")]
        self.ipv6_monitor_visibility = [ipaddress.ip_network(prefix.strip()) for prefix in args.ip_status_ipv6_monitor_visibility.split(",")]
        self.monitor_private_access = ipaddress.ip_network(args.ip_status_ipv4_monitor_private_network)
        if args.exporter_address:
            self.exporter_address = ipaddress.ip_address(args.exporter_address)
        else:
            self.exporter_address = None
        if args.ip_status_ipv4_monitor_visibility_exceptions != "":
            self.ipv4_monitor_visibility_exceptions = [ipaddress.ip_network(prefix.strip()) for prefix in args.ip_status_ipv4_monitor_visibility_exceptions.split(",")]
        else:
            self.ipv4_monitor_visibility_exceptions = None

        # Prometheus metrics to collect
        self.openstack_floating_ip_quota = Gauge("custom_openstack_neutron_floating_ip_quota",
                                                 "Floating IP quota for a project", ["project_id"])
        self.openstack_project_info = Gauge("custom_openstack_project_info",
                                            "Custom info for a project",
                                            ["project_id", "project_name", "domain_id", "project_tag"])
        self.openstack_server_info = Gauge("custom_openstack_server_info",
                                            "Custom info for a server",
                                            ["server_id", "server_name", "image_id", "flavor_name"])
        self.openstack_flavor_info = Gauge("custom_openstack_flavor_info",
                                            "Openstack flavor parameters",
                                            ["flavor_id", "flavor_name", "ram", "disk", "vcpus", "public", "ephemeral", "disabled"])
        self.openstack_loadbalancer_status = Gauge("custom_openstack_loadbalancer_accessibility_status",
                                                   "LoadBalancer accessibility status", ["id", "status"])
        self.openstack_ip_status = Gauge("custom_openstack_ip_accessibility_status",
                                                   "External (VLAN) IP accessibility status", ["id", "ip", "status"])

    def run_metrics_loop(self):
        """Metrics fetching loop"""

        while True:
            self.fetch_fip_quota()
            self.fetch_project_info()
            self.fetch_server_info()
            self.fetch_flavor_info()
            self.fetch_ip_status()
            self.fetch_loadbalancer_status()
            time.sleep(self.polling_interval_seconds)

    def fetch_fip_quota(self):
        """
        Get metrics from application and refresh Prometheus metrics with
        new values.
        """

        # remove all statuses
        self.openstack_floating_ip_quota.clear()

        # Update Prometheus metrics with application metrics
        for item in self.os_connection.network.quotas():
            self.openstack_floating_ip_quota.labels(project_id=item['project_id']).set(item['floating_ips'])

    def fetch_project_info(self):
        """
        Get metrics from application and refresh Prometheus metrics with
        new values.
        """

        # remove all statuses
        self.openstack_project_info.clear()

        for project in self.os_connection.identity.projects():
            for tag in project['tags']:
                self.openstack_project_info.labels(
                    project_id=project['id'],
                    project_name=project['name'],
                    domain_id=project['domain_id'],
                    project_tag=tag
                ).set(project['is_enabled'])
            if project['tags'] == []:
                self.openstack_project_info.labels(
                    project_id=project['id'],
                    project_name=project['name'],
                    domain_id=project['domain_id'],
                    project_tag=""
                ).set(project['is_enabled'])

    def fetch_server_info(self):
        """
        Get metrics from application and refresh Prometheus metrics with
        new values.
        """

        # remove all statuses
        self.openstack_server_info.clear()

        for server in self.os_connection.compute.servers(True,True):
            self.openstack_server_info.labels(
                server_id=server['id'],
                server_name=server['name'],
                image_id=server['image']['id'],
                flavor_name=server['flavor']['original_name']
            ).set(1)

    def fetch_flavor_info(self):
        """
        Get metrics from application and refresh Prometheus metrics with
        new values.
        """

        # remove all statuses
        self.openstack_flavor_info.clear()

        for flavor in self.os_connection.compute.flavors():
            self.openstack_flavor_info.labels(
                flavor_id=flavor['id'],
                flavor_name=flavor['name'],
                ram=flavor['ram'],
                disk=flavor['disk'],
                vcpus=flavor['vcpus'],
                public=flavor['os-flavor-access:is_public'],
                ephemeral=flavor['OS-FLV-EXT-DATA:ephemeral'],
                disabled=flavor['OS-FLV-DISABLED:disabled']
            ).set(1)

    # support function for ip status evaluation
    @staticmethod
    def get_ip_accessibility_status_ping(ip_address):
        ''' Try accessibility by pinging '''
        address = format(ip_address) if ip_address.version == 4 else "[{}]".format(ip_address)
        if ping(address, timeout=Constants.PING_TIMEOUT):
            return IPAddressStatus.REACHABLE.value

        return None

    # support function for ip status evaluation
    def get_ip_accessibility_status_ports(self, ip_address, ports):
        ''' Try accessibility by connecting to visible ports '''
        address = format(ip_address) if ip_address.version == 4 else "[{}]".format(ip_address)
        common_port_present = False

        for port in ports:
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                    sock.settimeout(Constants.CONNECTION_TIMEOUT)
                    sock.connect((address, port))
                AppMetrics.reachable_past_run[ip_address] = str(port)
                return IPAddressStatus.REACHABLE.value
            except Exception as ex:
                logging.info(f"{str(address)} | Connection to port {str(port)} failed with: {str(type(ex).__name__)}, description: {str(ex)}")
                if isinstance(ex, socket.timeout):
                    if port in Constants.PORTS_COMMON:
                        common_port_present = True
                    continue
                if isinstance(ex, ConnectionRefusedError):
                    AppMetrics.reachable_past_run[ip_address] = str(port)
                    return IPAddressStatus.REACHABLE.value

                return IPAddressStatus.UNKNOWN.value

        if common_port_present:
            return IPAddressStatus.UNREACHABLE.value
        return IPAddressStatus.HIDDEN.value

    def sg_rule_transparent(self, rule):
        ''' Returns True if the VM is reachable through the passed security group rule. '''
        # 'remote_group_id' = access is limited to a VM with a specific security group
        # 'port_range' = full port range isn't used for connecting with the internet
        if (
            (rule['direction'] != 'ingress') or
            (rule['remote_group_id'] is not None) or
            (rule['port_range_min'] is None) or
            (rule['port_range_min'] == '1' and rule['port_range_max'] == '65535')
           ):
            return False

        # remote_ip_prefix is 0/0 (valid) by default.
        if rule["remote_ip_prefix"] == "0.0.0.0/0" or rule["remote_ip_prefix"] is None:
            return True

        rule_remote_network = ipaddress.ip_network(rule["remote_ip_prefix"], strict=False)

        # VM access is limited to a subnet, which the exporter isn't part of.
        if self.exporter_address and self.exporter_address not in rule_remote_network:
            return False

        if rule_remote_network.version == 4:
            # Skip excepted (not visible) networks if any
            if self.ipv4_monitor_visibility_exceptions:
                for excepted_subnet in self.ipv4_monitor_visibility_exceptions:
                    if rule_remote_network.subnet_of(excepted_subnet):
                        return False
            # Skip if VM isn't visible for the exporter
            for visible_network in self.ipv4_monitor_visibility:
                #if rule_remote_network.subnet_of(visible_network):
                if rule_remote_network == visible_network:
                    return True
        elif rule_remote_network.version == 6:
            for visible_network in self.ipv6_monitor_visibility:
                #if rule_remote_network.subnet_of(visible_network):
                if rule_remote_network == visible_network:
                    return True

        return False

    def get_enabled_ports(self, security_groups):
        ''' Returns a set of enabled ports throughout all security groups and their rules. '''
        enabled_ports = set()

        for i_sg in security_groups:
            for rule in i_sg.security_group_rules:
                # Skip this rule if the exporter can't reach the VM through it.
                if self.sg_rule_transparent(rule) and rule['protocol'] in Constants.TCP_PROTOCOLS:
                    for i_port in Constants.PORTS_COMMON:
                        if i_port in range(rule['port_range_min'], (rule['port_range_max']) + 1):
                            enabled_ports.add(i_port)
                    # We don't want to add enormous number of ports, so the len() limit is used.
                    if ((rule['port_range_max'] is None or rule['port_range_min'] == rule['port_range_max']) and
                        len(enabled_ports) <= len(Constants.PORTS_COMMON) + Constants.PORTS_ADDITIONAL_NUM):
                        enabled_ports.add(rule['port_range_min'])

        return enabled_ports

    def protocols_in_any_security_group(self, security_groups, protocols):
        ''' Returns True if list of security groups (its rules) contain some protocol from protocols list. '''
        for i_sg in security_groups:
            for rule in i_sg.security_group_rules:
                # Skip this rule, if the exporter can't reach the VM through it.
                if self.sg_rule_transparent(rule):
                    for protocol in protocols:
                        if rule['protocol'] == protocol:
                            return True

        return False

    def get_valid_security_groups(self, vm):
        ''' Returns list of valid security groups for vm '''
        security_groups = []

        # catch in case VM has no security group attached - will be hidden
        for i_sg in self.security_groups:
            try:
                {'name': i_sg.name} in vm.security_groups
            except Exception:
                continue
            if i_sg.tenant_id == vm.project_id and {'name': i_sg.name} in vm.security_groups:
                security_groups.append(i_sg)

        return security_groups

    def get_accessibility_status(self, vm, address):
        ''' Ping and try to connect to connection ports. Return status (from IPAddressStatus class) of trials. '''
        valid_security_groups = self.get_valid_security_groups(vm)
        logging.debug(str(address) + " | Valid security groups: " + str(valid_security_groups))

        if not valid_security_groups:
            return IPAddressStatus.HIDDEN.value

        status = None
        if self.protocols_in_any_security_group(valid_security_groups, Constants.ICMP_PROTOCOLS):
            status = self.get_ip_accessibility_status_ping(address)
            logging.info(str(address) + " | Pinged with status: " + str(status))

        if status:
            AppMetrics.reachable_past_run[address] = 'ping'
        # Ping timed out, trying ports
        else:
            logging.info(str(address) + " | Ping not allowed or None")
            ports = self.get_enabled_ports(valid_security_groups)
            logging.info(str(address) + " | Enabled ports: " + str(ports))
            if not ports:
                return IPAddressStatus.HIDDEN.value
            status = self.get_ip_accessibility_status_ports(address, ports)

        return status

    def update_sg_and_vm_cache(self):
        ''' Call api for security group and virtual machine info and store into cache in case of api unavailability '''
        temp_security_groups = temp_virtual_machines = []
        try:
            with timeout.Timeout(self.api_timeout):
                temp_virtual_machines = list(self.os_connection.compute.servers(True, True))
                temp_security_groups = list(self.os_connection.network.security_groups())
        except Exception as timeout_error:
            print(timeout_error)

        if temp_virtual_machines and temp_security_groups:
            self.virtual_machines = []
            for vm in temp_virtual_machines:
                if vm.status == 'ACTIVE':
                    self.virtual_machines.append(vm)
            self.security_groups = temp_security_groups

    def fetch_ip_status(self):
        """
        Get IP availability status for each active vm and address reachable via monitoring,
        both public and private-muni addresses attached as FLAT networks are queried.
        """

        self.update_sg_and_vm_cache()
        fetch_ip_status = []
        logging.debug(f"Reachable in the previous run: {AppMetrics.reachable_past_run}")

        for vm in self.virtual_machines:
            for network in vm.addresses.items():
                for address_info in network[1]:
                    address = ipaddress.ip_address(address_info['addr'])
                    if address.is_private or address in self.monitor_private_access:
                        continue

                    ip_status = None
                    # Use past run test method/port if possible and skip some steps on this VM
                    try:
                        past_run = AppMetrics.reachable_past_run[address]
                        if past_run == 'ping':
                            ip_status = self.get_ip_accessibility_status_ping(address)
                        else:
                            ip_status = self.get_ip_accessibility_status_ports(address, [int(past_run)])
                    except KeyError:
                        pass # No record from past run, check VM normally

                    if ip_status == IPAddressStatus.REACHABLE.value:
                        logging.info(f"{str(address)} | Past run connection still reachable: {str(past_run)}")
                    else:
                        ip_status = self.get_accessibility_status(vm, address)
                    fetch_ip_status.append((vm.id, format(address), ip_status))
                    logging.info(str(address) + " | VM id: " + str(vm.id) + ", status: " + str(ip_status))

        self.openstack_ip_status.clear()
        for data in fetch_ip_status:
            self.openstack_ip_status.labels(id=data[0], ip=data[1], status=data[2]).set(1.0)

    def fetch_loadbalancer_status(self):
        """
        Get ip availability metric for each Octavia Loadbalancer and refresh Prometheus metrics with
        new values.
        """

        # remove all statuses (clears delete loadbalancers)
        self.openstack_loadbalancer_status.clear()
        # Reports LBs inaccessible via FIP
        try:
            for lb in self.os_connection.load_balancer.load_balancers():
                if lb.is_admin_state_up:
                    # catch partly created loadbalancers
                    ip_address = None
                    ports = []
                    try:
                        vip_port = self.os_connection.network.find_port(lb.vip_port_id)
                        sec_group = self.os_connection.network.find_security_group(vip_port.security_group_ids[0])
                        sg_rules = sec_group.security_group_rules
                        fips = list(self.os_connection.network.ips(port_id=lb.vip_port_id))
                        ip_address = ipaddress.ip_address(lb.vip_address)
                        if fips:
                            ip_address = ipaddress.ip_address(fips[0].floating_ip_address)

                        logging.debug(str(ip_address) + " | Loadbalancer security group: " + str(sec_group))

                        # ip address from private scopes, hidden from script
                        if ip_address.is_private:
                            self.openstack_loadbalancer_status.labels(
                                id=lb.id,
                                status=IPAddressStatus.HIDDEN.value
                            ).set(1.0)
                            continue
                        # check opened ports in health monitor range (1025+)
                        for sg_rule in sg_rules:
                            ports.append(sg_rule.get('port_range_min') or 0)
                        # add manually if missing - kypo fix
                        if not AppMetrics.loadbalancer_accessibility_ports[0] in ports:
                            ports.append(AppMetrics.loadbalancer_accessibility_ports[0])

                        logging.info(str(ip_address) + " | Loadbalancer ports: " + str(ports))

                    except Exception:
                        self.openstack_loadbalancer_status.labels(
                            id=lb.id,
                            status=IPAddressStatus.UNREACHABLE.value
                        ).set(1.0)
                        continue
                    lb_status = self.get_ip_accessibility_status_ports(
                        ip_address,
                        [port for port in ports if port in AppMetrics.loadbalancer_accessibility_ports]
                    )
                    if lb_status == IPAddressStatus.UNREACHABLE.value:
                        ping_status = self.get_ip_accessibility_status_ping(ip_address)
                        logging.info(str(ip_address) + " | Loadbalancer, ports unreachable, pinged with status: " + str(ping_status))
                        if ping_status:
                            lb_status = ping_status
                    logging.info(str(ip_address) + " | LB id: " + str(lb.id) + ", status: " + str(lb_status))
                    self.openstack_loadbalancer_status.labels(id=lb.id, status=lb_status).set(1.0)
        except ks_exc.EndpointNotFound:
            logging.info("Loadbalancer endpoint not found. Skipping loadbalancer checks...")

def parse_arguments(script_summary="script"):
    """ argument parser """

    parser = argparse.ArgumentParser(description=script_summary)
    parser.add_argument("--os-cloud", default=None,
                        help="OpenStack cloud name in clouds: in clouds.yaml file to use")
    parser.add_argument("--interval", default="60", type=int,
                        help="Polling interval in seconds, default %(default)s seconds")
    parser.add_argument("--address", default="0.0.0.0",
                        help="Address to which webserver binds to, default %(default)s")
    parser.add_argument("--port", default="9877", type=int,
                        help="Port to which webserver binds to, default %(default)s")
    parser.add_argument("--ip-status-api-timeout", default="120", type=int,
                        help="Timeout for API call, default: %(default)s")
    parser.add_argument("--ip-status-ipv6-monitor-visibility", default="::/0, 2001:718:801::/48",
                        help="IPv6 prefix visible from monitoring, default: %(default)s")
    parser.add_argument("--ip-status-ipv4-monitor-visibility", default="147.251.0.0/16, 10.16.0.0/16",
                        help="IPv4 prefix visible from monitoring, default: %(default)s")
    parser.add_argument("--ip-status-ipv4-monitor-visibility-exceptions", default="",
                        help="Subnets of --ip-status-ipv4-monitor-visibility considered unreachable, default: %(default)s")
    parser.add_argument("--ip-status-ipv4-monitor-private-network", default="10.16.0.0/16",
                        help="IPv4 private network accessible from monitoring, default: %(default)s")
    parser.add_argument("--exporter-address", default=None,
                        help="IP where the exporter runs, used for VM visibility.")
    parser.add_argument("--log-format", default='%(asctime)s %(levelname)s %(message)s',
                        help="Output format for log messages, default: %(default)s")
    parser.add_argument("--log-level", default="info",
                        help="Should debug logs be used?, default: %(default)s")
    script_args = parser.parse_args()

    if script_args.os_cloud is None:
        script_args.os_cloud = os.environ.get("OS_CLOUD", "openstack")

    return script_args


def main():
    """Main entry point"""

    args = parse_arguments(
        script_summary="custom-openstack-exporter.py custom openstack prometheus exporter"
    )

    logging.basicConfig(format=args.log_format, level=LOG_LEVELS[args.log_level], datefmt='%Y-%m-%d %H:%M:%S')

    # Unregister default metrics
    REGISTRY.unregister(PROCESS_COLLECTOR)
    REGISTRY.unregister(PLATFORM_COLLECTOR)

    # Unlike process and platform_collector that have no corresponding public named variable.
    REGISTRY.unregister(REGISTRY._names_to_collectors['python_gc_objects_collected_total'])

    app_metrics = AppMetrics(args)
    start_http_server(port=args.port, addr=args.address)
    app_metrics.run_metrics_loop()


if __name__ == "__main__":
    main()
